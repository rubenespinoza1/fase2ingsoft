package cl.ufro.dci;
import cl.ufro.dci.helpers.PdfReport;
import cl.ufro.dci.model.*;
import com.itextpdf.text.pdf.PdfWriter;
import org.junit.jupiter.api.*;
import com.itextpdf.text.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class PdfReportTest {

    @BeforeAll
    public static void setUpClass() {

    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {

    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testCrearReporte() throws IOException, DocumentException {
        Region region = new Region("Region de la Araucania");
        Comuna comuna = new Comuna("Temuco", region);
        ArrayList<Paciente> pacienteList = new ArrayList<>();
        ArrayList<PersonalMedico> personalList = new ArrayList<>();
        Establecimiento establecimiento = new Establecimiento("Hospital Hernan Henriquez Aravena",
                "Manuel Montt 115",comuna,pacienteList,personalList);
        EstadoPaciente estadoPaciente = new EstadoPaciente(true, true, "obesidad",
                "12/08/2020", "sedentario", "Tos persistente",
                     "Sospecha covid", "tos persistente, fiebre, diarrea" );
        Paciente paciente = new Paciente((long)18564879, "8", establecimiento,"Juan", "Perez",
                "Varón", "12/06/89", estadoPaciente, "Chileno",
                    "Temuco", "San Martín 989", "985457845" );
        Antecedente antecedente = new Antecedente((long)8784545, "20/05/2020",
                    "21/05/2020", 0, true, "Chile", "Temuco");
        PersonalMedico personalMedico = new PersonalMedico("Pedro", "Pereira",
                "987452145", "pedro.pereira@gmail.com", establecimiento);
        Enfermedad enfermedad = new Enfermedad((long)5465465, "EPOC", true);
        Caso caso = new Caso((long)54654545, "20/11/2020", "24/08/2015", paciente, enfermedad);
        PdfReport pdfReport = new PdfReport();
        Document document = new Document();
        FileOutputStream ficheroPdf = new FileOutputStream("informe.pdf");
        PdfWriter.getInstance(document,ficheroPdf).setInitialLeading(20);
        document.open();
        pdfReport.crearReporte(caso, paciente, establecimiento, personalMedico, region, estadoPaciente,
                antecedente);
    }
}
