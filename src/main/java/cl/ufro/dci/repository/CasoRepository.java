package cl.ufro.dci.repository;

import cl.ufro.dci.model.Caso;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CasoRepository extends CrudRepository<Caso,Long> {
}
