package cl.ufro.dci.repository;

import cl.ufro.dci.model.Comuna;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComunaRepository extends CrudRepository<Comuna, Long>  {
}
