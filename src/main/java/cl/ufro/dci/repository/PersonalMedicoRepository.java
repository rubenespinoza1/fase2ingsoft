package cl.ufro.dci.repository;

import cl.ufro.dci.model.PersonalMedico;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonalMedicoRepository extends CrudRepository<PersonalMedico, Long> {
}
