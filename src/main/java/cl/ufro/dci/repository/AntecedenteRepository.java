package cl.ufro.dci.repository;

import cl.ufro.dci.model.Antecedente;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AntecedenteRepository extends CrudRepository<Antecedente, Long> {

    List<Antecedente> findAll();

}
