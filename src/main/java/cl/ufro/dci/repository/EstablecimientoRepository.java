package cl.ufro.dci.repository;

import cl.ufro.dci.model.Establecimiento;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstablecimientoRepository extends CrudRepository<Establecimiento, Long> {

}
