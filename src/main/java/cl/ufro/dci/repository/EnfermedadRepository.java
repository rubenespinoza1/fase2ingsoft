package cl.ufro.dci.repository;

import cl.ufro.dci.model.Enfermedad;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface EnfermedadRepository extends CrudRepository<Enfermedad, Long> {
    Enfermedad findByEnfermNombre(String nombre);
}
