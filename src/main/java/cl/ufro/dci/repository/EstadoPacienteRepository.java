package cl.ufro.dci.repository;

import cl.ufro.dci.model.EstadoPaciente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoPacienteRepository extends CrudRepository<EstadoPaciente,Long> {
}
