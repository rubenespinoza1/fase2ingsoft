package cl.ufro.dci.helpers;

import cl.ufro.dci.model.*;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Clase helper destinada a conformar el informe que se envia junto a la muestra para el examen PCR de Covid-19.
 *
 *
 * @author Héctor Sepúlveda
 * @author Carlos Castillo
 */
public class PdfReport {

    /**
     * Método principal de la clase PdfReport, cuya salida es el informe para cuyo fin se creo esta clase.
     *
     *
     * @param caso Objeto de la clase Caso, de donde deberian sacarse todos los datos para este método.
     * @param paciente Objeto de la clase Paciente
     * @param establecimiento Objeto de la clase Establecimiento
     * @param personalMedico Objeto de la clase PersonalMedico
     * @param region Objeto de la clase Region
     * @param estadoPaciente Objeto de la clase estadoPaciente
     * @param antecedente Objeto de la clase antecedente
     */
    public void crearReporte(Caso caso, Paciente paciente, Establecimiento establecimiento,
            PersonalMedico personalMedico, Region region, EstadoPaciente estadoPaciente,
            Antecedente antecedente) throws FileNotFoundException, DocumentException, IOException {
        Document documento = new Document();
        FileOutputStream ficheroPdf = new FileOutputStream("informe.pdf");
        PdfWriter.getInstance(documento, ficheroPdf).setInitialLeading(20);
        documento.open();
        añadirEncabezado(documento);
        añadirEncabezado2(documento, "Datos Personales");
        añadirInfoPaciente(documento, paciente, establecimiento);
        añadirEncabezado2(documento, "Datos de la Procedencia");
        añadirInfoProcedencia(documento, personalMedico, establecimiento, region);
        añadirEncabezado2(documento, "Estado del Paciente");
        añadirEstadoPaciente(documento, estadoPaciente);
        documento.newPage();
        añadirEncabezado2(documento, "Antecedentes Clinico");
        añadirAntecedentesClinicos(documento, antecedente);
        documento.close();
    }

    private void añadirEncabezado(Document documento) throws IOException, DocumentException {
        Image imagen = Image.getInstance("MINSAL.png");
        imagen.scaleAbsolute(80, 80);
        imagen.setAbsolutePosition(100f, 755f);
        documento.add(imagen);
        Paragraph paragraph = new Paragraph("Formulario notificacion inmediata y envio de muestras"
                + "\na confirmacion IRA grave y 2019-nCoV",
                FontFactory.getFont(FontFactory.TIMES_BOLDITALIC, 16, Font.BOLDITALIC,
                        new BaseColor(0, 0, 0)));
        paragraph.setAlignment(Element.ALIGN_RIGHT);
        documento.add(paragraph);
        Chunk linebreak = new Chunk(new LineSeparator());
        documento.add(linebreak);
    }

    private void añadirEncabezado2(Document documento, String tituloEncabezado) throws DocumentException {
        Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
        Paragraph encabezado = new Paragraph(tituloEncabezado, boldFont);
        documento.add(encabezado);
        documento.add(Chunk.NEWLINE);
    }

    private void añadirInfoPaciente(Document documento, Paciente paciente, Establecimiento establecimiento) throws DocumentException {
        PdfPTable datosPersonales = new PdfPTable(1);
        datosPersonales.addCell("Nombre: " + paciente.getPacNombres() + " "
                + paciente.getPacApellidos() + "\n"
                + "Rut Paciente: " + String.valueOf(paciente.getPacRut()) + "\n"
                + "Dv Paciente: " + paciente.getPacDv() + "\n"
                + "Sexo: " + paciente.getPacSexo() + "\n"
                + "Fecha Nacimiento: " + paciente.getPacFechaNacimiento() + "?n"
                + "Nacionalidad: " + paciente.getPacNacionalidad() + "\n"
                + "Pueblo Originario: " + paciente.getPacPuebloOriginario() + "\n"
                + "Direccion: " + paciente.getPacDireccion() + "\n"
                + "Telefono: " + paciente.getPacTelefono() + "\n"
                + "Nombre Establecimiento: " + establecimiento.getEstNombre());
        documento.add(datosPersonales);
    }

    private void añadirInfoProcedencia(Document documento, PersonalMedico personalMedico,
            Establecimiento establecimiento, Region region) throws DocumentException {
        PdfPTable informacionProcedencia = new PdfPTable(1);
        informacionProcedencia.addCell("Nombre: " + personalMedico.getDocNombres() + " "
                + personalMedico.getDocApellidos() + "\n"
                + "Comuna: " + establecimiento.getComuna() + "\n"
                + "Region: " + region.getRgnNombre() + "\n"
                + "Dirección: " + establecimiento.getEstDireccion() + "\n"
                + "Laboratorio/Hospital: " + establecimiento.getEstNombre() + "\n"
                + "Correo Electronico: " + personalMedico.getDocEmail() + "\n"
                + "Telefono: " + personalMedico.getDocTelefono());
        documento.add(informacionProcedencia);
    }

    private void añadirEstadoPaciente(Document documento, EstadoPaciente estadoPaciente) throws DocumentException {
        PdfPTable informacionEstadoPaciente = new PdfPTable(1);
        String estadoVital = estadoVital(estadoPaciente);
        String estadoSintomatico = estadoSintomatico(estadoPaciente);
        String presentaSintomas = presentaSintomas(estadoPaciente);
        informacionEstadoPaciente.addCell("Se encuentra con vida: " + estadoVital + "\n"
                + "Razon del examen: " + estadoPaciente.getMotivoDeExamen() + "\n"
                + "¿Es Asintomatico?: " + estadoSintomatico + "\n"
                + "¿Presenta Sintomas?: " + presentaSintomas + "\n"
                + "Sintomas del Paciente: " + estadoPaciente.getSintomasPaciente() + "\n"
                + "Fecha de inicio de los sintomas: " + estadoPaciente.getFechaInicioSintomas() + "\n"
                + "Comorbilidades: " + estadoPaciente.getComorbilidades() + "\n"
                + "Estilo de vida: " + estadoPaciente.getEstiloDeVida() + "\n"
                + "Razon de sospecha: " + estadoPaciente.getRazonSospecha());
        documento.add(informacionEstadoPaciente);
    }

    private String estadoVital(EstadoPaciente estadoPaciente) {
        String estadoVital = "";
        if (estadoPaciente.isVivo() == true) {
            estadoVital = "Sí";
        } else {
            estadoVital = "No";
        }
        return estadoVital;
    }

    private String estadoSintomatico(EstadoPaciente estadoPaciente) {
        String estadoSintomatico = "";
        if (estadoPaciente.isSintomatico() == true) {
            estadoSintomatico = "Sí";
        } else {
            estadoSintomatico = "No";
        }
        return estadoSintomatico;
    }

    private String presentaSintomas(EstadoPaciente estadoPaciente) {
        String presentaSintomas = "";
        if (estadoPaciente.isSintomas() == true) {
            presentaSintomas = "Sí";
        } else {
            presentaSintomas = "No";
        }
        return presentaSintomas;
    }

    private void añadirAntecedentesClinicos(Document documento, Antecedente antecedente) throws DocumentException {
        PdfPTable antecedentesClinicos = new PdfPTable(1);
        String viajoExtranjero = viajoExtranjero(antecedente);
        if (viajoExtranjero.equalsIgnoreCase("no")) {
            antecedentesClinicos.addCell("Fecha de inicio de sintomas: " + antecedente.getFechaInicioSintomas() + "\n"
                    + "Fecha de la primera consulta: " + antecedente.getFechaPrimeraConsulta() + "\n"
                    + "Semanas de gestacion: " + antecedente.getSemanasGestacion() + "\n"
                    + "¿Viajo al extranjero?: " + viajoExtranjero);
            documento.add(antecedentesClinicos);
        } else {
            antecedentesClinicos.addCell("Fecha de inicio de sintomas: " + antecedente.getFechaInicioSintomas() + "\n"
                    + "Fecha de la primera consulta: " + antecedente.getFechaPrimeraConsulta() + "\n"
                    + "Semanas de gestacion: " + antecedente.getSemanasGestacion() + "\n"
                    + "¿Viajo al extranjero?: " + viajoExtranjero + "\n"
                    + "¿País al que viajó?; " + antecedente.getPais() + "\n"
                    + "¿Ciudad a la que viajó?: " + antecedente.getCiudad());
            documento.add(antecedentesClinicos);
        }
    }

    private String viajoExtranjero(Antecedente antecedente) {
        String viajoExtranjero = "";
        if (antecedente.isViajoExtranjero() == true) {
            viajoExtranjero = "Sí";
        } else {
            viajoExtranjero = "No";
        }
        return viajoExtranjero;
    }
}
