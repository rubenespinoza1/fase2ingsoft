package cl.ufro.dci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvU2Icc410Application {

    public static void main(String[] args) {
        SpringApplication.run(EvU2Icc410Application.class, args);
    }

}
