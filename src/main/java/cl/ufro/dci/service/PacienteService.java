package cl.ufro.dci.service;

import cl.ufro.dci.model.Paciente;
import cl.ufro.dci.repository.PacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PacienteService {

    @Autowired
    private PacienteRepository data;

    public List<Paciente> listar() {
        return (List<Paciente>) data.findAll();
    }

    public Optional<Paciente> listarRun(Long run) {
        return data.findById(run);
    }

    public int save(Paciente p) {
        int res = 0;
        Paciente paciente = data.save(p);
        if (!paciente.equals(null)){
            res=1;
        }
        return res;
    }

    public void delete(Long run) {
        data.deleteById(run);
    }
}
