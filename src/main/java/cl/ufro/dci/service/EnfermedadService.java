package cl.ufro.dci.service;

import cl.ufro.dci.model.Enfermedad;
import cl.ufro.dci.repository.EnfermedadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EnfermedadService {
    @Autowired
    private EnfermedadRepository enfermedadRepository;

    public Iterable<Enfermedad> listar() {
        return  enfermedadRepository.findAll();
    }
    public Enfermedad save(Enfermedad enfermedad) {
        Enfermedad enf = enfermedadRepository.save(enfermedad);
        return enf;
    }
    public void delete(Long enfId) {
        enfermedadRepository.deleteById(enfId);
    }
    public boolean isExist(Long enfId){
        return enfermedadRepository.existsById(enfId);
    }
    public Optional<Enfermedad> findById(Long enfId){
        return enfermedadRepository.findById(enfId);
    }
    public Enfermedad findByEnfermedadName(String enfName){
        return enfermedadRepository.findByEnfermNombre(enfName);
    }

}
