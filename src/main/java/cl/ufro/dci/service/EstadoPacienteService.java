package cl.ufro.dci.service;

import cl.ufro.dci.model.EstadoPaciente;
import cl.ufro.dci.repository.EstadoPacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EstadoPacienteService {
    @Autowired
    private EstadoPacienteRepository data;

    public List<EstadoPaciente> listar() {

        return (List<EstadoPaciente>) data.findAll();
    }

    public Optional<EstadoPaciente> listarId(Long id) {

        return data.findById(id);
    }

    public int save(EstadoPaciente ep) {
        int res = 0;
        EstadoPaciente estado_paciente = data.save(ep);
        if (!estado_paciente.equals(null)){
            res=1;
        }
        return res;
    }

    public void delete(Long id) {

        data.deleteById(id);
    }
}
