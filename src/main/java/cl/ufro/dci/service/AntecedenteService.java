package cl.ufro.dci.service;

import cl.ufro.dci.model.Antecedente;
import cl.ufro.dci.repository.AntecedenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AntecedenteService {

    @Autowired
    AntecedenteRepository antecedenteRepository;

    //Obtener una lista de todos los antecedentes
    public List<Antecedente> obtenerAntecedente(){

        return antecedenteRepository.findAll();
    }

    //Crear un antecedente
    public Antecedente crearAntecedente(Antecedente a){

        return antecedenteRepository.save(a);
    }

    //Obtener un antecedente
    public Antecedente encontrarAntecedente(Long id){
        Optional<Antecedente> optionalAntecedente = antecedenteRepository.findById(id);
        if (optionalAntecedente.isPresent()){
            return optionalAntecedente.get();
        }else{
            return null;
        }
    }

    //Edición de un antecedente
    public Antecedente editarAntecedente(Long id, String fechaInicioSintomas, String fechaPrimeraConsulta, int semanasGestacion, boolean viajoExtranjero, String pais, String ciudad){

        if(antecedenteRepository.findById(id).isPresent()){

            Antecedente antecedente = antecedenteRepository.findById(id).get();

            antecedente.setFechaInicioSintomas(fechaInicioSintomas);
            antecedente.setFechaPrimeraConsulta(fechaPrimeraConsulta);
            antecedente.setSemanasGestacion(semanasGestacion);
            antecedente.setViajoExtranjero(viajoExtranjero);
            antecedente.setPais(pais);
            antecedente.setCiudad(ciudad);

            return antecedenteRepository.save(antecedente);

        }

        return null;
    }

    //Eliminar antecedente
    public void eliminarAntecedente(Long id){
        antecedenteRepository.deleteById(id);
    }

}
