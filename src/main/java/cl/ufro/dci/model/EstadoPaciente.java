package cl.ufro.dci.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "estadoPaciente")
public class EstadoPaciente {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id_estado")
    private Long idEstado;

    @Column(name = "vivo")
    private boolean vivo;

    @Column(name = "sintomatico")
    private boolean sintomatico;

    @Column(name = "fecha_inicio_sintomas")
    private String fechaInicioSintomas;

    @Column(name = "comorbilidades")
    private String comorbilidades;

    @Column(name = "estilo_de_vida")
    private String estiloDeVida;

    @Column(name = "razon_sospecha")
    private String razonSospecha;

    @Column(name = "motivo_examen")
    private String motivoDeExamen;

    @Column(name = "sintomas_paciente")
    private String sintomasPaciente;

    public EstadoPaciente(){}

    public EstadoPaciente(boolean vivo, boolean sintomas, String comorbilidades, String fechaInicioSintomas, String estiloDeVida, String razonSospecha, String motivoDeExamen, String sintomasPaciente){
        this.vivo=vivo;
        this.sintomatico=sintomas;
        this.comorbilidades = comorbilidades;
        this.estiloDeVida = estiloDeVida;
        this.razonSospecha = razonSospecha;
        this.motivoDeExamen = motivoDeExamen;
        this.sintomasPaciente = sintomasPaciente;
        this.fechaInicioSintomas=fechaInicioSintomas;
    }

    public EstadoPaciente(Long idEstado, boolean vivo, boolean sintomas,String comorbilidades,String fechaInicioSintomas, String estiloDeVida, String razonSospecha, String motivoDeExamen, String sintomasPaciente){
        this.idEstado=idEstado;
        this.vivo=vivo;
        this.sintomatico=sintomas;
        this.comorbilidades = comorbilidades;
        this.estiloDeVida = estiloDeVida;
        this.razonSospecha = razonSospecha;
        this.motivoDeExamen = motivoDeExamen;
        this.sintomasPaciente = sintomasPaciente;
        this.fechaInicioSintomas=fechaInicioSintomas;
    }

    public boolean isSintomatico() {
        return sintomatico;
    }

    public void setSintomatico(boolean sintomatico) {
        this.sintomatico = sintomatico;
    }

    public String getComorbilidades() {
        return comorbilidades;
    }

    public void setComorbilidades(String comorbilidades) {
        this.comorbilidades = comorbilidades;
    }

    public String getEstiloDeVida() {
        return estiloDeVida;
    }

    public void setEstiloDeVida(String estiloDeVida) {
        this.estiloDeVida = estiloDeVida;
    }

    public String getRazonSospecha() {
        return razonSospecha;
    }

    public void setRazonSospecha(String razonSospecha) {
        this.razonSospecha = razonSospecha;
    }

    public String getMotivoDeExamen() {
        return motivoDeExamen;
    }

    public void setMotivoDeExamen(String motivoDeExamen) {
        this.motivoDeExamen = motivoDeExamen;
    }

    public String getSintomasPaciente() {
        return sintomasPaciente;
    }

    public void setSintomasPaciente(String sintomasPaciente) {
        this.sintomasPaciente = sintomasPaciente;
    }

    public Long getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Long idEstado) {
        this.idEstado = idEstado;
    }

    public boolean isVivo() {
        return vivo;
    }

    public void setVivo(boolean vivo) {
        this.vivo = vivo;
    }

    public boolean isSintomas() {
        return sintomatico;
    }

    public void setSintomas(boolean sintomas) {
        this.sintomatico = sintomas;
    }

    public String getFechaInicioSintomas() {
        return fechaInicioSintomas;
    }

    public void setFechaInicioSintomas(String fechaInicioSintomas) {
        this.fechaInicioSintomas = fechaInicioSintomas;
    }

}
