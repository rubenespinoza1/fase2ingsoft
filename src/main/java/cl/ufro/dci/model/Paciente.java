package cl.ufro.dci.model;
import javax.persistence.*;

@Entity
@Table(name = "paciente")
public class Paciente {


    @Id
    @Column(name = "pac_rut")
    private Long pacRut;

    @Column(name = "pac_dv")
    private String pacDv;

    @ManyToOne
    @JoinColumn(name = "est_id")
    private Establecimiento establecimiento;

    @Column(name = "pac_nombres")
    private String pacNombres;

    @Column(name = "pac_apellidos")
    private String pacApellidos;

    @Column(name = "pac_sexo")
    private String pacSexo;

    @Column(name = "pac_fecha_nacimiento")
    private String pacFechaNacimiento;

    @Column(name = "pac_nacionalidad")
    private String pacNacionalidad;

    @Column(name = "pac_pueblo_originario")
    private String pacPuebloOriginario;

    @Column(name = "pac_direccion")
    private String pacDireccion;

    @Column(name = "pac_telefono")
    private String pacTelefono;

    @ManyToOne
    @JoinColumn(name="pac_id_estado")
    private EstadoPaciente pacEstado;


    public Paciente() {
    }

    /**
     * Construtor de la clase Paciente, variante con todos parametros obligatorios para su registro en la base de datos
     * @param pacRut RUN sin el digito verificador del paciente
     * @param pacDv Digito verificador del RUN del paciente
     * @param establecimiento
     * @param pacNombres Nombres del paciente
     * @param pacApellidos Apellidos paterno y materno del paciente
     * @param pacSexo Sexo del paciente, segun su preferencia
     * @param pacFechaNacimiento Fecha de nacimiento del paciente
     * @param pacNacionalidad Nacionalidad del paciente
     * @param pacPuebloOriginario Pueblo originario del paciente
     * @param pacDireccion Direccion de la residencia del paciente
     * @param pacTelefono Telefono celular del paciente
     */
    public Paciente(Long pacRut, String pacDv, Establecimiento establecimiento, String pacNombres, String pacApellidos,
                    String pacSexo, String pacFechaNacimiento, EstadoPaciente pacEstado, String pacNacionalidad, String pacPuebloOriginario,
                    String pacDireccion, String pacTelefono) {
        this.pacRut = pacRut;
        this.pacDv = pacDv;
        this.establecimiento = establecimiento;
        this.pacNombres = pacNombres;
        this.pacApellidos = pacApellidos;
        this.pacSexo = pacSexo;
        this.pacFechaNacimiento = pacFechaNacimiento;
        this.pacNacionalidad = pacNacionalidad;
        this.pacPuebloOriginario = pacPuebloOriginario;
        this.pacDireccion = pacDireccion;
        this.pacTelefono = pacTelefono;
        this.pacEstado=pacEstado;
    }

    public Long getPacRut() {
        return pacRut;
    }

    public void setPacRut(Long pacRut) {
        this.pacRut = pacRut;
    }

    public String getPacDv() {
        return pacDv;
    }

    public void setPacDv(String pacDv) {
        this.pacDv = pacDv;
    }

    public String getPacNombres() {
        return pacNombres;
    }

    public void setPacNombres(String pacNombres) {
        this.pacNombres = pacNombres;
    }

    public String getPacApellidos() {
        return pacApellidos;
    }

    public void setPacApellidos(String pacApellidos) {
        this.pacApellidos = pacApellidos;
    }

    public Establecimiento getEstablecimiento() {
        return establecimiento;
    }

    public void setEstablecimiento(Establecimiento establecimiento) {
        this.establecimiento = establecimiento;
    }

    public String getPacSexo() {
        return pacSexo;
    }

    public void setPacSexo(String pacSexo) {
        this.pacSexo = pacSexo;
    }

    public String getPacFechaNacimiento() {
        return pacFechaNacimiento;
    }

    public void setPacFechaNacimiento(String pacFechaNacimiento) {
        this.pacFechaNacimiento = pacFechaNacimiento;
    }

    public String getPacNacionalidad() {
        return pacNacionalidad;
    }

    public void setPacNacionalidad(String pacNacionalidad) {
        this.pacNacionalidad = pacNacionalidad;
    }

    public String getPacPuebloOriginario() {
        return pacPuebloOriginario;
    }

    public void setPacPuebloOriginario(String pacPuebloOriginario) {
        this.pacPuebloOriginario = pacPuebloOriginario;
    }

    public String getPacDireccion() {
        return pacDireccion;
    }

    public void setPacDireccion(String pacDireccion) {
        this.pacDireccion = pacDireccion;
    }

    public String getPacTelefono() {
        return pacTelefono;
    }

    public void setPacTelefono(String pacTelefono) {
        this.pacTelefono = pacTelefono;
    }
}
