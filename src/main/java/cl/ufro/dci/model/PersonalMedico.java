package cl.ufro.dci.model;

import javax.persistence.*;

@Entity
@Table(name = "personal_medico")
public class PersonalMedico {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "docId")
    private Long docId;

    @Column(name = "doc_nombres")
    private String docNombres;

    @Column(name = "doc_apellidos")
    private String docApellidos;

    @Column(name = "doc_telefono")
    private String docTelefono;

    @Column(name = "doc_email")
    private String docEmail;

    @ManyToOne
    @JoinColumn(name = "est_id")
    private Establecimiento establecimiento;

    public PersonalMedico(){}

    public PersonalMedico(String docNombres, String docApellidos, String docTelefono, String docEmail, Establecimiento establecimiento){
        this.docNombres=docNombres;
        this.docApellidos=docApellidos;
        this.docEmail = docEmail;
        this.docTelefono = docTelefono;
        this.establecimiento=establecimiento;
    }

    public PersonalMedico(Long docId, String docNombres, String docApellidos,String docTelefono, String docEmail, Establecimiento establecimiento){
        this.docId=docId;
        this.docNombres=docNombres;
        this.docApellidos=docApellidos;
        this.docEmail = docEmail;
        this.docTelefono = docTelefono;
        this.establecimiento=establecimiento;
    }

    public String getDocTelefono() {
        return docTelefono;
    }

    public void setDocTelefono(String docTelefono) {
        this.docTelefono = docTelefono;
    }

    public String getDocEmail() {
        return docEmail;
    }

    public void setDocEmail(String docEmail) {
        this.docEmail = docEmail;
    }

    public Long getDocId() {
        return docId;
    }

    public void setDocId(Long docId) {
        this.docId = docId;
    }

    public String getDocNombres() {
        return docNombres;
    }

    public void setDocNombres(String docNombres) {
        this.docNombres = docNombres;
    }

    public String getDocApellidos() {
        return docApellidos;
    }

    public void setDocApellidos(String docApellidos) {
        this.docApellidos = docApellidos;
    }

}
