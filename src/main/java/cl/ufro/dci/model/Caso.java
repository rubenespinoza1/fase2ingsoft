package cl.ufro.dci.model;

import javax.persistence.*;

@Entity
@Table(name = "casos")
public class Caso {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "caso_id")
    private Long idSolicitud;

    @Column(name = "fecha_actualizacion")
    private String fechaActualizacion;

    @Column(name = "fecha_creacion")
    private String fechaCreacion;

    @ManyToOne
    @JoinColumn(name = "pac_rut")
    private Paciente paciente;

    @ManyToOne
    @JoinColumn(name = "enferm_id")
    private Enfermedad enfermedad;
//
//    @Column(name = "antecedente")
//    private Antecedente antecedente;
//
//    @Column(name = "comuna")
//    private Comuna comuna;
//
//    @Column(name = "region")
//    private Region region;
//
//    @Column(name = "establecimiento")
//    private Establecimiento establecimiento;
//
//    @Column(name = "estadoPaciente")
//    private EstadoPaciente estadoPaciente;

    public Caso(){}

    public Caso(Long idSolicitud, String fechaActualizacion, String fechaCreacion, Paciente paciente, Enfermedad enfermedad) {
        this.idSolicitud = idSolicitud;
        this.fechaActualizacion = fechaActualizacion;
        this.fechaCreacion = fechaCreacion;
        this.paciente=paciente;
        this.enfermedad=enfermedad;
    }

    public Long getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(Long idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public String getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(String fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
}
