package cl.ufro.dci.model;


import javax.persistence.*;

@Entity
@Table(name = "enfermedad")
public class Enfermedad {


    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "enferm_id")
    private Long enfermId;

    @Column(name = "enferm_nombre")
    private String enfermNombre;

    @Column(name = "inmediata")
    private boolean inmediata;

    public Enfermedad(){}

    public Enfermedad(Long enfermId, String enfermNombre, boolean inmediata){
        this.enfermId=enfermId;
        this.enfermNombre=enfermNombre.toUpperCase();
        this.inmediata = inmediata;
    }

    public Enfermedad(String enfermNombre, boolean inmediata) {
        this.enfermNombre = enfermNombre.toUpperCase();
        this.inmediata = inmediata;
    }

    public Enfermedad(String enfermNombre){
        this.enfermNombre=enfermNombre.toUpperCase();
    }

    public Long getEnfermId() {
        return enfermId;
    }

    public void setEnfermId(Long enfermId) {
        this.enfermId = enfermId;
    }

    public String getEnfermNombre() {
        return enfermNombre;
    }

    public void setEnfermNombre(String enfermNombre) {
        this.enfermNombre = enfermNombre;
    }

    public boolean isInmediata() {
        return inmediata;
    }

    public void setInmediata(boolean inmediata) {
        this.inmediata = inmediata;
    }
}

