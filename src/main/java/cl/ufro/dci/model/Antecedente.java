package cl.ufro.dci.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "antecedente")
public class Antecedente {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "ant_id")
    private Long id;

    @Column(name = "fecha_inicio_sintomas")
    private String fechaInicioSintomas;

    @Column(name = "fecha_primera_consulta")
    private String fechaPrimeraConsulta;


    @Column(name = "semanas_gestacion")
    private int semanasGestacion;

    @Column(name = "viajo_extranjero")
    private boolean viajoExtranjero;

    @Column(name = "pais")
    private String pais;

    @Column(name = "ciudad")
    private String ciudad;

    //@OneToOne(mappedBy = "antecedente")
    //private Paciente paciente;

    public Antecedente(String fechaInicioSintomas, String fechaPrimeraConsulta, int semanasGestacion, boolean viajoExtranjero, String pais, String ciudad){}

    public Antecedente(Long id, String fechaInicioSintomas, String fechaPrimeraConsulta, int semanasGestacion, boolean viajoExtranjero, String pais, String ciudad) {
        this.id = id;
        this.fechaInicioSintomas = fechaInicioSintomas;
        this.fechaPrimeraConsulta = fechaPrimeraConsulta;
        this.semanasGestacion = semanasGestacion;
        this.viajoExtranjero = viajoExtranjero;
        this.pais = pais;
        this.ciudad = ciudad;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFechaInicioSintomas() {
        return fechaInicioSintomas;
    }

    public void setFechaInicioSintomas(String fechaInicioSintomas) {
        this.fechaInicioSintomas = fechaInicioSintomas;
    }

    public String getFechaPrimeraConsulta() {
        return fechaPrimeraConsulta;
    }

    public void setFechaPrimeraConsulta(String fechaPrimeraConsulta) {
        this.fechaPrimeraConsulta = fechaPrimeraConsulta;
    }

    public int getSemanasGestacion() {
        return semanasGestacion;
    }

    public void setSemanasGestacion(int semanasGestacion) {
        this.semanasGestacion = semanasGestacion;
    }

    public boolean isViajoExtranjero() {
        return viajoExtranjero;
    }

    public void setViajoExtranjero(boolean viajoExtranjero) {
        this.viajoExtranjero = viajoExtranjero;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
}