package cl.ufro.dci.controller;

import cl.ufro.dci.model.EstadoPaciente;
import cl.ufro.dci.service.EstadoPacienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping
public class EstadoPacienteController {

    @Autowired
    private EstadoPacienteService service;

    @GetMapping("/listadoEstadoP")
    public List<EstadoPaciente> listar_estado(Model model){
        List<EstadoPaciente> estadoPacientes = service.listar();
        model.addAttribute("estadoPacientes",estadoPacientes);
        return estadoPacientes;
    }

    @GetMapping("/listadoEstadoP/{id}")
    public String editar_estado(@PathVariable Long id, Model model){
        Optional<EstadoPaciente> paciente = service.listarId(id);
        model.addAttribute("paciente",paciente);
        return "paciente";
    }

    @PostMapping("/guardarEstadoP")
    public String guardar_estado(EstadoPaciente ep){
        service.save(ep);
        return "redirect:/listadoEstadoP";
    }

    @GetMapping("/borrarEstadoP/{id}")
    public String borrar_estadp (Model model, @PathVariable Long id){
        service.delete(id);
        return "redirect:/listadoEstadoP";
    }

}
