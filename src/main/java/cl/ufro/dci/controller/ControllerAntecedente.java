package cl.ufro.dci.controller;

import cl.ufro.dci.model.Antecedente;
import cl.ufro.dci.service.AntecedenteService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ControllerAntecedente {

    private AntecedenteService antecedenteService;

    public ControllerAntecedente(AntecedenteService antecedenteService){
        this.antecedenteService = antecedenteService;
    }

    //Obtener todos los antencedentes
    @RequestMapping("/cotroller/antecedentes")
    public List<Antecedente> obtenerAntecedentes(){
        return antecedenteService.obtenerAntecedente();
    }

    //Crear nuevos antecedentes
    @RequestMapping(value="/controller/antecedentes", method = RequestMethod.POST)
    public Antecedente crearAntecedente(@RequestParam(value="fechaInicioSintomas") String fechaInicioSintomas, @RequestParam(value="fechaPrimeraConsulta") String fechaPrimeraConsulta, @RequestParam(value="semanasGestacion") int semanasGestacion, @RequestParam(value="viajoExtranjero") boolean viajoExtranjero, @RequestParam(value="pais") String pais, @RequestParam(value="ciudad") String ciudad){
        Antecedente antecedente = new Antecedente(fechaInicioSintomas, fechaPrimeraConsulta, semanasGestacion,viajoExtranjero, pais, ciudad);
        return antecedenteService.crearAntecedente(antecedente);
    }

    //Obtener antecedentes por id
    @RequestMapping("/controller/antecedentes/{id}")
    public Antecedente mostrarAntecedenteId(@PathVariable("id") Long id){
        Antecedente antecedente = antecedenteService.encontrarAntecedente(id);
        return antecedente;
    }

    //Actualizar antecedente por id
    @RequestMapping(value="/controller/antecedentes/{id}", method = RequestMethod.PUT)
    public Antecedente modificarAntecedente(@PathVariable("id") Long id, @RequestParam(value="fechaInicioSintomas") String fechaInicioSintomas, @RequestParam(value="fechaPrimeraConsulta") String fechaPrimeraConsulta, @RequestParam(value="semanasGestacion") int semanasGestacion, @RequestParam(value="viajoExtranjero") boolean viajoExtranjero, @RequestParam(value="pais") String pais, @RequestParam(value="ciudad") String ciudad){
        Antecedente antecedente = antecedenteService.editarAntecedente(id, fechaInicioSintomas, fechaPrimeraConsulta, semanasGestacion,viajoExtranjero, pais, ciudad);
        return antecedente;
    }

    //Eliminar antecedente por id
    @RequestMapping(value="/controller/antecedentes/{id}", method = RequestMethod.DELETE)
    public void eliminarAntecedenteId(@PathVariable("id") Long id){
        antecedenteService.eliminarAntecedente(id);
    }


}
