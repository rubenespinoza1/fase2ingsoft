package cl.ufro.dci.controller;

import cl.ufro.dci.model.Enfermedad;
import cl.ufro.dci.service.EnfermedadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


@RestController
@CrossOrigin
@RequestMapping("/api/enfermedades/")
public class EnfermedadController {
    @Autowired
    private EnfermedadService eService;

    @GetMapping("")
    public Iterable<Enfermedad> listarEnfermedades() {
        return eService.listar();
    }

    @PostMapping("agregar")
    public ResponseEntity<String> agregarEnfermedad(@RequestBody Enfermedad enfermedad) {
        Enfermedad enfermedadDB = null;
        String nombreUp = enfermedad.getEnfermNombre().toUpperCase();
        enfermedad.setEnfermNombre(nombreUp);
        try {
            enfermedadDB = buscarEnfermedadPorNombre(enfermedad.getEnfermNombre());
        } catch (Exception e) {
        }
        if (enfermedadDB == null) {
            eService.save(enfermedad);
            String mensaje = "La enfermedad: " + enfermedad.getEnfermNombre() + " fue agregada.";
            return ResponseEntity.ok(mensaje);
        } else {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "La enfermedad ya existe.");
        }
    }

    @GetMapping("buscar/nombre={nombre}")
    public Enfermedad buscarEnfermedadPorNombre(@PathVariable String nombre) {
        String nombreUp = nombre.toUpperCase();
        Enfermedad enfermedad = null;
        try {
            enfermedad = eService.findByEnfermedadName(nombreUp);
        } catch (Exception e) {
        }
        if (enfermedad == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Enfermedad no encontrada.");
        } else {
            return enfermedad;
        }
    }
}