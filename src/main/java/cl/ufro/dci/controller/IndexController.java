package cl.ufro.dci.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/demo")
public class IndexController {

    /**
     * You can try this controller in http://localhost:8080/api/demo/saludar
     * @return
     */
    @GetMapping("/saludar")
    public String sayHelloWorld(){
        return "Hello World!";
    }
}
