package cl.ufro.dci.controller;

import cl.ufro.dci.model.Paciente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import cl.ufro.dci.service.PacienteService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping
public class PacienteController {
    @Autowired
    private PacienteService service;

    @GetMapping("/listarpacientes")
    public List<Paciente> listar_paciete(Model model){
        List<Paciente>pacientes = service.listar();
        model.addAttribute("pacientes",pacientes);
        return pacientes;
    }

    @GetMapping("/nuevopaciente")
    public String agregar_paciente(Model model){
        model.addAttribute("paciente", new Paciente());
        return "paciente";
    }
    @PostMapping("/guardarpaciente")
    public String guardar_paciente(Paciente p){
        service.save(p);
        return "redirect:/listarpacientes";
    }

    @GetMapping("/editar/{run}")
    public String editar_paciente(@PathVariable Long run, Model model){
        Optional<Paciente> paciente = service.listarRun(run);
        model.addAttribute("paciente",paciente);
        return "paciente";
    }
    @GetMapping("/listarpaciente/{run}")
    public Optional<Paciente> listar_paciente_id(@PathVariable Long run, Model model){
        Optional<Paciente> paciente = service.listarRun(run);
        model.addAttribute("paciente",paciente);
        return paciente;
    }

    @GetMapping("/borrarpaciente/{run}")
    public String borrar_paciente(Model model, @PathVariable Long run){
        service.delete(run);
        return "redirect:/listar";
    }

}